import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:reg_midterm/User.dart';
import 'Reg.dart';
import 'User.dart';

class Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100,
        backgroundColor: Color.fromARGB(255, 231, 180, 85),
        leading: (
          Image (image: AssetImage('asset/images/buu.png'))
        ),
        title: (
          Text ('REG BUU' ,style: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white))
        ),
        actions: [
          IconButton(
          icon: const Icon(Icons.menu,color: Color.fromARGB(255, 94, 92, 92)),
          iconSize: 40,
          onPressed: () {
            Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Reg()),
            );
          }),
          IconButton(
          icon: const Icon(Icons.account_circle),
          iconSize: 45,
          onPressed: () {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => User()),
            );
          }),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
            children: [
              Container(
                width: 800,
                height: 800,
                decoration: BoxDecoration(
                color: Colors.yellow[200],
                borderRadius: BorderRadius.circular(0.0),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 172, 171, 171),
                    blurRadius: 10,
                    spreadRadius: 0,
                    offset: Offset(2,4)
                  )
                ]),
            child: Column(
              children: [
                Stack(children: [
                  QuickAccessbtn(),
                ],)
              ],
            )


              ),
            ],
        ),
      ),
    );

  }
}

Widget QuickAccessbtn(){
    return Container( 
      child : Column( 
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ลงทะเบียน',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
          
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ผลการลงทะเบียน',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
         
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ผลอนุมัติเพิ่ม-ลด',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
          
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ตารางเรียน/สอบ',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
                    Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ประวัตินิสิต',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),

                    Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ภาระค่าใช้จ่ายทุน',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),

                    Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ผลการศึกษา',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),

                    Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ตรวจสอบจบ',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),

                    Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('รายชื่อนิสิต',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),


          const SizedBox(
              height: 50,
          ),
          
            Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 232, 81, 81),
              padding: const EdgeInsets.symmetric(horizontal: 120, vertical: 20),
            ),
            onPressed: () {},
            icon: Icon(Icons.logout,size: 30, color: Color.fromARGB(255, 0, 0, 0),),
            label: Text('ออกจากระบบ', style: TextStyle(fontSize: 18, color: Color.fromARGB(255, 0, 0, 0),fontWeight: FontWeight.bold))
          ),
          ),
          

          
        ]
    ),
  );
  }