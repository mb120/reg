import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:reg_midterm/User.dart';
import 'Menu.dart';


class Reg extends StatelessWidget {
  bool userOpen = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100,
        backgroundColor: Color.fromARGB(255, 231, 180, 85),
        leading: Sign(),
        title: (
          Text ('REG BUU' ,style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold, color: Colors.white))
        ),
        actions: [
          IconButton(
          icon: const Icon(Icons.menu),
          iconSize: 40,
          onPressed: () {
            Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Menu()),
            );
          }),
          userOpen?IconButton(
          icon: const Icon(Icons.account_circle),
          iconSize: 45,
          onPressed: () {
            userOpen = true;
            Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => User()),
            );
          }):IconButton(
          icon: const Icon(Icons.account_circle,color: Color.fromARGB(255, 255, 255, 255),),
          iconSize: 45,
          onPressed: () {
            userOpen = false;
            Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => User()),
            );
          }),

        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(children: [
              Information(),
              Click()
              ]),
            QuickAccess(),
            const SizedBox(
              height: 20,
            ),
            QuickAccessbtn(),
             const SizedBox(
              height: 40,
            ),
            contact(),
            
        ]
        ),
      ),
      
    );
  }

  Widget Click(){
    return Column(
      children: [
        SizedBox(
            height: 230,
          ),
        Row(
        children: [
          SizedBox(
            width: 140,
          ),
          Directionality(
            textDirection: TextDirection.rtl,
            child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    fixedSize: const Size(100, 50),
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)
                    ),
                    backgroundColor: Color.fromARGB(255, 231, 180, 85),
                    padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
                  ),
                  onPressed: (){},
                  label: Text('CLICK',style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 54, 48, 6))),
                  icon: Icon(Icons.chevron_left,size: 20, color: Colors.black,)
                ),
          ),]
      ),
      Row(
        children: [
          SizedBox(
            width: 135,
          ),
          Text('•',style: const TextStyle(fontSize: 80, fontWeight: FontWeight.bold, color: Colors.yellow)),
          Text('•',style: const TextStyle(fontSize: 80, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 206, 205, 203))),
          Text('•',style: const TextStyle(fontSize: 80, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 206, 205, 203))),
          Text('•',style: const TextStyle(fontSize: 80, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 206, 205, 203))),
        ],
      ),
      ]
    );
  }


  Widget Information(){
    return SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  Image(image: AssetImage('asset/images/1.png'),width: 395),
                  Image(image: AssetImage('asset/images/2.png'),width: 395),
                  Image(image: AssetImage('asset/images/3.png'),width: 395),
                  Image(image: AssetImage('asset/images/4.png'),width: 395),
                ]
            ),
    );
  }

  Widget QuickAccess() {
  return SingleChildScrollView( 
        child: Row( 
          mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(padding: const EdgeInsets.all(5.0)),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 0), //apply padding horizontal or vertical only
            child: Text("Quick Access",style: TextStyle( 
                  fontSize: 25,
                  height: 1.2, //line height 150% of actual height
                  color: Color.fromARGB(255, 54, 48, 6),
                  fontWeight: FontWeight.bold, 
              ),  
          ),
          ),
        
        ]
        ),
      );
  }

  Widget QuickAccessbtn(){
    return Container( 
      child : Column( 
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ทำบัตรนิสิตรูปแบบใหม่',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
          
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('เสนอความคิดเห็น',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
         
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ยื่นคำร้อง',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
          
          Container(
            padding: EdgeInsets.only(top: 2.0),
            child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              alignment: Alignment.bottomLeft,
              fixedSize: const Size(600, 60),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0)
              ),
              backgroundColor: Color.fromARGB(255, 206, 205, 203),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.chevron_right,size: 30, color: Colors.deepOrangeAccent,),
            label: Text('ประวัติการเข้าชม',textAlign: TextAlign.left, style: const TextStyle(fontSize: 18, color: Colors.black,))
          ),
          ),
          
          const SizedBox(
              height: 20,
          ),
          Container( 
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: SizedBox(
                  width: 170,
                  height: 40,
                  child: ElevatedButton(
                    child: const Text('VIEW ALL', style: const TextStyle(fontSize: 18, color: Colors.black,)),
                      style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)
                      ),
                      elevation: 1,
                      backgroundColor: Color.fromARGB(255, 213, 128, 43),
                    ),
                    onPressed: () {
                    },
                  ),
                )
            ),
        ]
    ),
  );
  }

  Widget contact(){
    return Container(
              height: 260,
              color: Color.fromARGB(255, 231, 180, 85),
              child: Center(
                child: Column(children: <Widget>[
                  Container(padding: const EdgeInsets.all(4)),
                  Text("ติดต่อเรา", style: const TextStyle(fontSize: 15, color: Colors.black)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                       const SizedBox(
                        width: 20,
                      ),
                      Icon(Icons.location_on,color: Color.fromARGB(255, 213, 128, 43),),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("169 ถนน ลงหาดบางแสน ตำบลแสนสุข\nอำเภอเมืองชลบุรี ชลบุรี 20131", style: const TextStyle(fontSize: 15, color: Colors.black)),
  
                    ]
                  ),
                  const SizedBox(
                        height: 10,
                      ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(
                        width: 20,
                      ),
                      Icon(Icons.email,color: Color.fromARGB(255, 213, 128, 43),),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("administrator@buu.ac.th", style: const TextStyle(fontSize: 15, color: Colors.black)),
                    ]
                  ),
                  const SizedBox(
                        height: 10,
                      ),
                  Text("ติดตามเรา", style: const TextStyle(fontSize: 15, color: Colors.black)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(
                        width: 20,
                      ),
                      Icon(Icons.facebook,color: Color.fromARGB(255, 213, 128, 43),),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("Facebook", style: const TextStyle(fontSize: 15, color: Colors.black)),
                    ]
                  ),
                    const SizedBox(
                        height: 10,
                      ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(
                        width: 20,
                      ),
                      Icon(Icons.web_asset,color: Color.fromARGB(255, 213, 128, 43),),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("Website", style: const TextStyle(fontSize: 15, color: Colors.black)),
                    ]
                  ),
                    const SizedBox(
                        height: 10,
                      ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(
                        width: 20,
                      ),
                      Icon(Icons.forum ,color:Color.fromARGB(255, 213, 128, 43),),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("Line", style: const TextStyle(fontSize: 15, color: Colors.black)),
                    ]
                  ),
                  
                ]),
              ),
    );
  }

  Widget Sign(){
    return Container( 
          child: Positioned(
            left: 100,
            child: Image (image: AssetImage('asset/images/buu.png'),),
          )
        );
  }

}