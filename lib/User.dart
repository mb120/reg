import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:reg_midterm/User.dart';
import 'Reg.dart';
import 'Menu.dart';

class User extends StatelessWidget {
    bool userOpen = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100,
        backgroundColor: Color.fromARGB(255, 231, 180, 85),
        leading: (
          Image (image: AssetImage('asset/images/buu.png'))
        ),
        title: (
          Text ('REG BUU' ,style: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white))
        ),
        actions: [
          IconButton(
          icon: const Icon(Icons.menu),
          iconSize: 40,
          onPressed: () {
                       userOpen = true;
            Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Menu()),
            );
          }),
          IconButton(
          icon: const Icon(Icons.account_circle,color: Color.fromARGB(255, 94, 92, 92)),
          iconSize: 45,
          onPressed: () {
            userOpen = true;
            Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Reg()),
            );
            
          }),

        ],
      ),
      backgroundColor: Colors.yellow[200],
      body: SingleChildScrollView(
        child: Column(
          children: [
          Container(
            width: 400,
            height: 550,
            decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
            borderRadius: BorderRadius.circular(0.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 172, 171, 171),
                blurRadius: 10,
                spreadRadius: 0,
                offset: Offset(2,4)
              )
            ]),
            child: Column(
              children:[
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                     CircleAvatar(
                        radius: 80,
                        backgroundImage: AssetImage(
                          'asset/images/5.jpg'
                        ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Text('63160198@go.buu.ac.th', style: TextStyle(color: Color.fromARGB(255, 131, 128, 128), fontSize: 18)),
              SizedBox(
                  height: 20,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 20,
                 ),
                  Text('ชื่อ : ธนภรณ์ นนทิสิทธิ์', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),

              SizedBox(
                  height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 20,
                 ),
                  Text('คณะ : วิทยาการสารสนเทศ', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),

              SizedBox(
                  height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 20,
                 ),
                  Text('หลักสูตร : 2115020: วท.บ. (วิทยาการคอม', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),

              SizedBox(
                  height: 5,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 95,
                 ),
                  Text('พิวเตอร์) ปรับปรุง : 59 - ป.ตรี 4 ปี', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),

              SizedBox(
                  height: 5,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 95,
                 ),
                  Text('ปกติ : วิชาโท : 0:-', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),

              SizedBox(
                  height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 20,
                 ),
                  Text('สถานภาพ : กำลังศึกษา', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),

              SizedBox(
                  height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 20,
                 ),
                  Text('อ.ที่ปรึกษา : อาจารย์ภูสิต กุลเกษม, ผู้ช่วย', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),

              SizedBox(
                  height: 5,
              ),
              Row(
                children: [
                  SizedBox(
                  width: 107,
                 ),
                  Text('ศาสตราจารย์ ดร. โกเมศ อัมพวัน', style: TextStyle(color: Colors.black, fontSize: 18)),
                ],
              ),


              ],
            ),
          ), 
        ]
        ),
      ),  
    );
  }

}
