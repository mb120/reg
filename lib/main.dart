import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:reg_midterm/Menu.dart';
import 'package:reg_midterm/User.dart';
import 'Reg.dart';
import 'login.dart';
import 'Menu.dart';

void main() {
  runApp(DevicePreview(
    enabled: true,
    builder: (BuildContext context) => const MyApp(
    ),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       debugShowCheckedModeBanner: false,
      title: "Reg BUU ",
      home: LoginScreen(),
    );
  }
}
